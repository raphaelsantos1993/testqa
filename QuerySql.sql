/*
Tabelas:

PROFESSOR
codp PK
nome
fone

DISCIPLINA
codd PK
nome
codp FK

CURSA
codd FK
coda FK
ano

ALUNO
coda PK
nome
*/

/*Escreva a consulta SQL para listar o nome de todos os alunos
matriculados na   disciplina de Cálculo do professor João.*/
SELECT ALUNO.nome FROM ALUNO
INNER JOIN CURSA ON ALUNO.coda = CURSA.coda
INNER JOIN DISCIPLINA ON DISCIPLINA.codd = CURSA.codd
INNER JOIN PROFESSOR ON DISCIPLINA.codp = PROFESSOR.codp
WHERE DISCIPLINA.nome = 'Cálculo' AND PROFESSOR.nome = 'João';
GROUP BY ALUNO.nome;

/*• Escreva a consulta SQL para exibir a quantidade de alunos por disciplinas.*/
SELECT DISCIPLINA.nome , COUNT(CURSA.coda) AS QUANTIDADE
FROM DISCIPLINA
INNER JOIN CURSA ON DISCIPLINA.codd = CURSA.codd
GROUP BY DISCIPLINA.nome;

/*• Escreva a consulta SQL para listar as disciplinas que todos os professores lecionam.*/
SELECT DISCIPLINA.nome , COUNT(DISCIPLINA.codp) AS QUANTIDADE FROM DISCIPLINA
INNER JOIN PROFESSOR ON PROFESSOR.codp = DISCIPLINA.codp
WHERE QUANTIDADE = (SELECT COUNT(*) FROM PROFESSOR)
GROUP BY DISCIPLINA.nome;

/*• Escreva a consulta SQL que exibe o total de professores.*/
SELECT COUNT(*) FROM PROFESSOR;

/*• Escreva a consulta SQL para listar todos os alunos que cursaram alguma
disciplina do ano 2000 até 2020.*/
SELECT ALUNO.nome ,CURSA.ano FROM ALUNO
INNER JOIN CURSA ON ALUNO.coda = CURSA.coda
WHERE CURSA.ano BETWEEN 2000 AND 2020
GROUP BY ALUNO.nome, CURSA.ano DESC;