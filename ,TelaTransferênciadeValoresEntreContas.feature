#language: pt

Funcionalidade: Validar Tela Transferência de Valores entre Contas 

Contexto: Esta logado na tela de Transferência de Valores entre Contas
Dado que estou logado na tela de Transferência de Valores entre Contas

Cenário: Validar a presença do saldo na Tela
Dado que estou na Transferência de Valores entre Contas
Então o saldo da conta deve ser exibido na tela

Cenário: Validar componentes da tela Transferência de Valores entre Contas
Dado que estou na Transferência de Valores entre Contas
Então os campos Cliente Destino, Valor e Data da Efetivação devem ser editáveis 
E os buttons Transferir e Cancelar devem ser clicáveis 

Cenário: Validar componente button Transferir da tela Transferência de Valores entre Contas ativo
Dado que estou na Transferência de Valores entre Contas
E os campos Cliente Destino, Valor e Data da Efetivação estão preenchidos
Então o button Transferir deve ser clicável 

Cenário: Validar componente button Transferir da tela Transferência de Valores entre Contas inativo
Dado que estou na Transferência de Valores entre Contas
E os campos Cliente Destino, Valor e Data da Efetivação não estão preenchidos
Então o button Transferir não deve ser clicável

Cenário: Validar componente button Cancelar da tela Transferência de Valores entre Contas ativo com campos anteriores preenchidos
Dado que estou na Transferência de Valores entre Contas
E os campos Cliente Destino, Valor e Data da Efetivação estão preenchidos
Então o button Cancelar deve ser clicável
E após o clique deve ser exibida a Tela inicial do app

Cenário: Validar componente button Cancelar da tela Transferência de Valores entre Contas com campos anteriores não preenchidos
Dado que estou na Transferência de Valores entre Contas
E os campos Cliente Destino, Valor e Data da Efetivação não estão preenchidos
Então o button Cancelar deve ser clicável
E após o clique deve ser exibida a Tela inicial do app

Cenário: Validar transferência com sucesso
Dado que estou na Transferência de Valores entre Contas
E os campos Cliente Destino, Valor e Data da Efetivação estão preenchidos com valores corretos
E clico no button Transferir
Então é exibida a mensagem de sucesso na transferência

Cenário: Validar falha na transferência com cliente incorreto
Dado que estou na Transferência de Valores entre Contas
E o campo Cliente Destino está preenchido com <cliente> inexistente
E os campos Valor e Data da Efetivação estão preenchidos com valores corretos
E clico no button Transferir
Então deve ser exibida a mensagem de Falha na transferência

 Exemplos:
    |    cliente   |
    |  "asdasdsa"  |
    |   1234567    |
    |  "%$#&*@?!"  |

Cenário: Validar falha na transferência com Valor incorreto
Dado que estou na Transferência de Valores entre Contas
E o campo Cliente Destino está preenchido de forma correta
E o campo Valor está preenchido com <valor> incorreto
E o campo Data da Efetivação está preenchido de forma correta
E clico no button Transferir
Então deve ser exibida a mensagem de Falha na transferência

 Exemplos:
    |    valor     |
    |  "asdasdsa"  |
    |  R$1000,00   |
    |  R$-1000,00  |

Cenário: Validar falha na transferência com Data da Efetivação incorreta
Dado que estou na Transferência de Valores entre Contas
E o campo Cliente Destino está preenchido de forma correta
E o campo Valor está preenchido de forma correta
E o campo Data da Efetivação está preenchido com <data> incorreta
E clico no button Transferir
Então deve ser exibida a mensagem de Falha na transferência

 Exemplos:
    |    data      |
    |  "asdasdsa"  |
    |   12347849   |
    |  01*01*1111  |
    |  01-01-1111  |
    |  01/01/1970  |
    |  01/13/2000  |
    |  32/12/2022  |

Cenário: Validar a presença da atualização de saldo na Tela
Dado que estou na Transferência de Valores entre Contas
E o saldo da conta deve ser exibido na tela
E clico no button Transferir
E é exibida a mensagem de sucesso na transferência
Então o saldo da conta exibido na tela deve ser atualizado

Cenário: Validar componente button Cancelar da tela Transferência de Valores durante o processamento da transferência inativo
Dado que estou na Transferência de Valores entre Contas
E os campos Cliente Destino, Valor e Data da Efetivação não estão preenchidos
E clico no button Transferir
Então o button Cancelar não deve ser clicável enquanto não aparecer a mensagem de feedback na tela

# A escrita dos cenários de teste foram escritos e BDD (Gherkin)
# utilizei a Palavra reservada Contexto para realizar todos os passos anteriores até o usuário chegar na tela Transferência de Valores entre Contas
# Utilizei como premissa que após clicar no botão Cancelar deve ser voltar para tela anterior
# Utilizei como premissa que não é possível Cancelar uma transferência em andamento até termino do seu processamento